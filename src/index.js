import { canvas, ctx } from "./canvas"
import Particle from "./class/Particle"
import { RADIUS, TOTAL_CIRCLES } from "./constants"
import { COLOR_PALLETES } from "./constants/colors"
import { getRandomValueFromArray, randomIntFromInterval } from "./utils"


const particles = []
for(let i=0; i<TOTAL_CIRCLES; i++){
    particles.push(new Particle({
        x: randomIntFromInterval(RADIUS, canvas.width - RADIUS) , 
        y: randomIntFromInterval(RADIUS, canvas.height - RADIUS) , 
        radius:RADIUS, 
        color:getRandomValueFromArray(COLOR_PALLETES)
    }))
}

function animate(){
    requestAnimationFrame(animate)
    ctx.clearRect(0,0,canvas.width, canvas.height)
    
    particles.forEach(p=>p.update())
}

animate()

